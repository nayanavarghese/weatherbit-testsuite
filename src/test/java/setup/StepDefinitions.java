package setup;

import cucumber.api.java8.En;
import io.restassured.http.ContentType;
import io.restassured.response.ResponseBody;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import java.util.List;

import static io.restassured.RestAssured.given;

public class StepDefinitions implements En{

    private static final String BASE_URL = "https://api.weatherbit.io/v2.0";
    private String apiToken;
    private String countryCodeActual;
    private List<String> timestampUtcListActual;
    private List<String> weatherDataListActual;

    public StepDefinitions() {

        Given("^API Token (.*)$", (String token) -> {
            if(StringUtils.isBlank(token)) {
                throw new RuntimeException("Invalid API token");
            }
            apiToken=token;
        });

        When("^/current api is called with lat as (.*) and long as (.*)$", (String lat, String lon) -> {
            List<String> countryCodeActualList  = given().baseUri(BASE_URL)
                    .header("Accept" ,"application/json")
                    .queryParam("lat", lat)
                    .queryParam("lon", lon)
                    .queryParam("key", apiToken)
                    .contentType(ContentType.JSON)
                    .get("/current").then().
                    extract().jsonPath().getList("data.country_code");
            countryCodeActual = countryCodeActualList.get(0);
            System.out.println(countryCodeActual);
        });

        Then("^Should get State code as (.*)$", (String countryCodeExpected) -> {
            Assert.assertEquals(" Country Code didn't match", countryCodeExpected, countryCodeActual);
        });

        When("^/forecast/daily api is called with postal_code (.*)$", (String postalCode) -> {
            ResponseBody response = given().baseUri(BASE_URL)
                    .header("Accept" ,"application/json")
                    .queryParam("postal_code", postalCode)
                    .queryParam("key", apiToken)
                    .contentType(ContentType.JSON)
                    .get("/forecast/daily").body();
            timestampUtcListActual = response.jsonPath().getList("data.datetime");
            weatherDataListActual = response.jsonPath().getList("data.weather");
            System.out.println(timestampUtcListActual);
            System.out.println(weatherDataListActual);

        });

        Then("^Should get valid WeatherData and datetime$", () -> {
            //Assert.assertEquals(" Country Code didn't match", countryCodeExpected, countryCodeActual);
        });
    }
}
